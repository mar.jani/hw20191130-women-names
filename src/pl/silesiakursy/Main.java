package pl.silesiakursy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    /*
            Napisz program który pobierze liczbe x
            Program zapyta o podanie x imion
            Program wyświetli tylko imiona kobiet (imiona które kończą się na 'a')
    */
    public static void main(String[] args) throws Exception {
        int numberOfFirstNames = getNumberOfFirstNames();
        List<String> names = getFristNames(numberOfFirstNames);
        showFemaleNames(names);
    }

    private static int getNumberOfFirstNames() throws Exception {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj liczbę imion:");
        int firstNameCount = 0;
        try {
            firstNameCount = scanner.nextInt();
        } catch (Exception e) {
            throw new Exception("Błędna wartość");
        }
        return firstNameCount;
    }

    private static List<String> getFristNames(int numberOfFirstNames) throws Exception {
        int count = 0;

        List<String> names = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        while (count < numberOfFirstNames) {
            try {
                System.out.println("Podaj " + (count + 1) + " imię:");
                names.add(scanner.nextLine());
                count++;
            } catch (Exception e) {
                throw new Exception("Błąd przy wprowadzaniu imienia");
            }
        }
        return names;
    }

    private static void showFemaleNames(List<String> names) {
        System.out.println("-----------------------------");
        System.out.println("Imiona żeńskie to:");
        for (String name : names) {
            if (checkNameAsFemale(name)) {
                System.out.println(name);
            }
        }
    }

    private static boolean checkNameAsFemale(String name) {
        if (name.length() > 0 && name.charAt(name.length() - 1) == 'a') {
            return true;
        }
        return false;
    }
}
